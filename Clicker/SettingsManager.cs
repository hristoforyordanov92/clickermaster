﻿using Newtonsoft.Json;
using System.IO;

namespace Clicker
{
    /// <summary>
    /// Manager class of the global settings of the tool.
    /// </summary>
    public class SettingsManager
    {
        const string settingsPath = @"settings";
        const string settingsFile = @"settings.fork";

        private static Settings _currentSettings;
        /// <summary>
        /// The current settings used by the tool.
        /// </summary>
        public static Settings CurrentSettings
        {
            get
            {
                if (_currentSettings == null)
                    _currentSettings = LoadSettings();

                return _currentSettings;
            }
        }

        /// <summary>
        /// Load the saved settings from a file. If no settings were saved, new settings with their default values will be created.
        /// </summary>
        public static Settings LoadSettings()
        {
            string settingsFilePath = Path.Combine(settingsPath, settingsFile);
            if (!File.Exists(settingsFilePath))
                return new Settings();

            string settingsFileContent = File.ReadAllText(settingsFilePath);
            return JsonConvert.DeserializeObject<Settings>(settingsFileContent);
        }

        /// <summary>
        /// Save the currently used settings to a file.
        /// </summary>
        public static void SaveSettings()
        {
            Directory.CreateDirectory(settingsPath);
            string settingsFilePath = Path.Combine(settingsPath, settingsFile);
            string serializedSettings = JsonConvert.SerializeObject(CurrentSettings);
            File.WriteAllText(settingsFilePath, serializedSettings);
        }
    }
}

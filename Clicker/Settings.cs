﻿using Newtonsoft.Json;
using System.Windows.Media;

namespace Clicker
{
    public class Settings
    {
        [JsonProperty("ClicksPerSecond")]
        public int ClicksPerSecond { get; set; } = 30;

        [JsonProperty("AutoClickZoneColor")]
        public Color AutoClickZoneColor { get; set; } = Color.FromArgb(50, 255, 255, 255);
    }
}

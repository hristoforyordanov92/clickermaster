﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using Gma.System.MouseKeyHook;

namespace Clicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string noSelectedWindow = "<NO SELECTED WINDOW>";

        private readonly IKeyboardMouseEvents _keyboardHook = Hook.GlobalEvents();

        private static Dispatcher dispatcher;

        private bool _isClickingEnabled = false;

        private ClickThroughWindow _clickthroughWindow;

        private IntPtr _selectedWindow = IntPtr.Zero;

        private Dictionary<string, IntPtr> availableWindows;

        public MainWindow()
        {
            InitializeComponent();

            dispatcher = Dispatcher.CurrentDispatcher;

            availableWindows = new Dictionary<string, IntPtr>();
            availableWindows.Add(noSelectedWindow, IntPtr.Zero);

            foreach (KeyValuePair<string, IntPtr> item in WindowsServices.GetAvailableWindows())
                availableWindows.Add(item.Key, item.Value);

            AvailableWindowsComboBox.ItemsSource = availableWindows.Keys;
            AvailableWindowsComboBox.SelectedIndex = 0;
            AvailableWindowsComboBox.SelectionChanged += AvailableWindowsComboBox_SelectionChanged;

            _keyboardHook.KeyPress += OnKeyboardKeyPress;

            ClicksPerSecond.Text = SettingsManager.CurrentSettings.ClicksPerSecond.ToString();
            ColorPicker.SelectedColor = SettingsManager.CurrentSettings.AutoClickZoneColor;

            ClicksPerSecond.TextChanged += ClicksPerSecond_TextChanged;
            ColorPicker.SelectedColorChanged += ColorPicker_SelectedColorChanged;
        }

        private void AvailableWindowsComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (availableWindows.TryGetValue((string) e.AddedItems[0], out IntPtr windowHandle))
            {
                _selectedWindow = windowHandle;
            }
        }

        private void ColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            if (!e.NewValue.HasValue)
                return;

            SettingsManager.CurrentSettings.AutoClickZoneColor = e.NewValue.Value;

            if (_clickthroughWindow == null)
                return;

            _clickthroughWindow.Background = new SolidColorBrush(SettingsManager.CurrentSettings.AutoClickZoneColor);
        }

        private bool _isResizingWindow = false;
        private System.Drawing.Point _initialWindowPoint = new System.Drawing.Point(0, 0);

        public static System.Drawing.Point Subtract(System.Drawing.Point a, System.Drawing.Point b)
        {
            return new System.Drawing.Point(a.X - b.X, a.Y - b.Y);
        }
        public static System.Drawing.Point Add(System.Drawing.Point a, System.Drawing.Point b)
        {
            return new System.Drawing.Point(a.X + b.X, a.Y + b.Y);
        }

        private void WindowResizing()
        {
            Task.Run(async () =>
            {
                while (_isResizingWindow)
                {
                    if (_clickthroughWindow == null)
                        return;

                    System.Drawing.Point cursorPosition = System.Windows.Forms.Cursor.Position;
                    System.Drawing.Point subtraction = Subtract(cursorPosition, _initialWindowPoint);

                    dispatcher.Invoke(() =>
                    {
                        if (subtraction.Y <= 0)
                        {
                            _clickthroughWindow.Top = cursorPosition.Y;
                            _clickthroughWindow.Height = Math.Abs(subtraction.Y);
                        }
                        else
                        {
                            _clickthroughWindow.Top = _initialWindowPoint.Y;
                            _clickthroughWindow.Height = subtraction.Y;
                        }

                        if (subtraction.X <= 0)
                        {
                            _clickthroughWindow.Left = cursorPosition.X;
                            _clickthroughWindow.Width = Math.Abs(subtraction.X);
                        }
                        else
                        {
                            _clickthroughWindow.Left = _initialWindowPoint.X;
                            _clickthroughWindow.Width = subtraction.X;
                        }
                    });

                    await Task.Delay(1000 / 50);
                }
            });
        }

        private void OnKeyboardKeyPress(object sender, KeyPressEventArgs e)
        {
            AutoclickerLogic(e);
            WindowResizingLogic(e);
            CloseBorderWindow(e);
        }

        private void CloseBorderWindow(KeyPressEventArgs e)
        {
            if (e.KeyChar != '[')
                return;

            if (_isResizingWindow)
                return;

            _clickthroughWindow?.Close();
            _clickthroughWindow = null;
        }

        private void WindowResizingLogic(KeyPressEventArgs e)
        {
            if (e.KeyChar != ']')
                return;

            if (_clickthroughWindow == null)
            {
                _clickthroughWindow = new ClickThroughWindow
                {
                    WindowStyle = WindowStyle.None,
                    Background = new SolidColorBrush(SettingsManager.CurrentSettings.AutoClickZoneColor),
                    Width = 1,
                    Height = 1,
                    AllowsTransparency = true,
                    ResizeMode = ResizeMode.NoResize,
                    ShowInTaskbar = false,
                    Topmost = true,
                };
            }

            _isResizingWindow = !_isResizingWindow;

            if (_isResizingWindow)
            {
                System.Drawing.Point cursorPosition = System.Windows.Forms.Cursor.Position;
                _initialWindowPoint = cursorPosition;
                _clickthroughWindow.Top = _initialWindowPoint.Y;
                _clickthroughWindow.Left = _initialWindowPoint.X;
                _clickthroughWindow.Show();
                WindowResizing();
            }
        }

        private void AutoclickerLogic(KeyPressEventArgs e)
        {
            if (e.KeyChar != '.')
                return;

            _isClickingEnabled = !_isClickingEnabled;
            ClicksPerSecond.IsEnabled = !_isClickingEnabled;
            if (_isClickingEnabled)
            {
                StartClickingTask();
            }
        }

        public void DisposeKeyboardHook()
        {
            _keyboardHook.KeyPress -= OnKeyboardKeyPress;

            _keyboardHook.Dispose();
        }

        private void StartClickingTask()
        {
            Task.Run(async () =>
            {
                while (_isClickingEnabled)
                {
                    bool canClick = true;
                    System.Drawing.Point cursorPosition = System.Windows.Forms.Cursor.Position;

                    if (_clickthroughWindow != null)
                    {
                        dispatcher.Invoke(() =>
                        {
                            if (_clickthroughWindow.Top > cursorPosition.Y || _clickthroughWindow.Top + _clickthroughWindow.Height < cursorPosition.Y ||
                                _clickthroughWindow.Left > cursorPosition.X || _clickthroughWindow.Left + _clickthroughWindow.Width < cursorPosition.X)
                            {
                                canClick = false;
                            }
                        });
                    }

                    if (_selectedWindow != IntPtr.Zero && WindowsServices.GetForegroundWindow() != _selectedWindow)
                        canClick = false;

                    if (canClick)
                        SimWinInput.SimMouse.Click(MouseButtons.Left, cursorPosition.X, cursorPosition.Y);

                    await Task.Delay(1000 / SettingsManager.CurrentSettings.ClicksPerSecond);
                }
            });
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            SettingsManager.SaveSettings();

            base.OnClosing(e);

            ClicksPerSecond.TextChanged -= ClicksPerSecond_TextChanged;
            ColorPicker.SelectedColorChanged -= ColorPicker_SelectedColorChanged;

            DisposeKeyboardHook();
            _clickthroughWindow?.Close();
        }

        private void ClicksPerSecond_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            var tb = (System.Windows.Controls.TextBox)e.OriginalSource;

            if (!int.TryParse(tb.Text, out int clicks))
                tb.Text = SettingsManager.CurrentSettings.ClicksPerSecond.ToString();

            if (clicks <= 0)
                tb.Text = "1";

            if (clicks > 1000)
                tb.Text = "1000";

            SettingsManager.CurrentSettings.ClicksPerSecond = clicks;
        }
    }
}
